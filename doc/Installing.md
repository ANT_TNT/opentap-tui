# Installing
Either build it from source or install it from the OpenTAP package repository ([packages.opentap.io](packages.opentap.io)).

## Install from Repository
- Install: `tap package install TUI --version any`
- Run: `tap tui`

## From Source
- Build: `dotnet build`
- Run: `OpenTAP.TUI/bin/Debug/tap tui`